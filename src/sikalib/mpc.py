"""
Multi program communications - MPC
"""

import os

class Transport(object):
    def __init__(self, path_in, path_out):
        if not os.path.exists(path_in):
            f = open(path_in, 'w')
            f.close()

        self._fin = open(path_in)
        self._fout = open(path_out, 'w')

    def read(self):
        self._fin.seek(0)
        return self._fin.read()

    def write(self, data):
        self._fout.seek(0)
        self._fout.write(data)
        self._fout.truncate()
        self._fout.flush()


class Peer(object):
    def __init__(self):
        self._transport = None

    @staticmethod
    def serialize(d):
        return '\n'.join('%s:%s' % it for it in d.items())

    @staticmethod
    def deserialize(r):
        d = {}
        if len(r) == 0:
            return {}
        for l in r.split('\n'):
            k, v = l.split(':')
            d[k] = v
        return d

    def raw_read(self):
        return self._transport.read()

    def raw_write(self, data):
        self._transport.write(data)

    def read(self):
        return self.deserialize(self.raw_read())

    def write(self, data):
        self.raw_write(self.serialize(data))


class Client(Peer):
    def __init__(self, path):
        Peer.__init__(self)
        self._transport = Transport(path+'-out', path+'-in')


class Server(Peer):
    def __init__(self, path):
        Peer.__init__(self)
        self._transport = Transport(path+'-in', path+'-out')
